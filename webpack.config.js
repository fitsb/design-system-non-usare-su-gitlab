const path  = require('path');
const autoprefixer = require('autoprefixer');
const htmlWebpackPlugin = require('html-webpack-plugin');
const miniCssExtractPlugin = require('mini-css-extract-plugin');
const webpack = require('webpack');

module.exports = {
    entry: {
        app: [
            path.join(__dirname + '/src/index.js')
        ]
    },
    output: {
        path: path.join(__dirname + '/dist'),
        filename: 'bundle.js'
    },
    devServer: {
        host: '0.0.0.0',
        port: 8080,
        open: true
    },
    module: {
        rules: [
            {
                test: /\.(sa|c|sc)ss$/,
                use: [
                    miniCssExtractPlugin.loader,
                    'css-loader',
                    'sass-loader'
                ]
            },
            {
                test: /\.(jpg|png|jpeg|svg)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: './src/img/[name].[ext]',
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        new htmlWebpackPlugin({
            template: './index.html',
            minify: {
                collapseWhitespace: false,
                removeComments: true,
                removeRedundantAttributes: false,
                removeScriptTypeAttributes: false,
                removeStyleLinkTypeAttributes: false,
                useShortDoctype: false
            }
        }),
        new miniCssExtractPlugin({
            filename: 'main.css'
        }),
        new webpack.LoaderTargetPlugin({
            options: {
                postcss: [
                    autoprefixer()
                ]
            }
        })
    ]
};