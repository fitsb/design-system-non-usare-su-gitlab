/**
 * @author Gian Pumayalla
 *
 * @param settings.element contenitore di tutti le accordion
 * @param settings.closedOnStart (optional) Al caricamento le accordion saranno chiuse
 * @param settings.openClass (optional) Il nome della classe che aprirà la accordion
 *
 * */

class Accordion {
    constructor(settings) {
        this.defaults = {
            'closedOnStart': true,
            'openClass': 'is-open',
        };

        this.settings = settings;
        this.element = this.settings.element;

        this.init();
    }

    init(){
        if(this.element){
            console.log("Accordion selezionato", this.element);
        } else {
            throw new Error("Accordion: devi prima selezionare un elemento");
        }

        if(!this.settings){
            this.settings = this.defaults;
        }
    }
}

export default Accordion;